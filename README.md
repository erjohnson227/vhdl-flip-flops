# VHDL Flop Flop Library

## Purpose
This library was designed to give VHDL based flip-flop primitives to a HDL designer for the development of HDL circuits. IDEs like Xilinx vivado do not provide flip-flop primitives and therefore these may be of some value to a developer. 

## What's Included

This repository contains a library of various types of Latch primitives, as well as a testbench for each primitive. The testbench and VHDL modules are compiled and executed via an included Makefile which serves as a simple testbench suite. 

If run on a computer with GHDL installed (and compiled to use LLVM) the makefile can be executed directly. For convenience, a submodule has been added named `ghdl-docker-container` which provides a docker container for the user which will run on any Windows machine without the need to install or compile GHDL (this is done automatically in the Dockerfile during image creation). 


## Running the Testbench Suite 

### Prerequisites
Follow the instructions [here](https://bitbucket.org/erjohnson227/ghdl-docker-container/src/master/README.md) to install all pre-requisites for running the docker development container. 

### Install Git for Windows

Git is required to clone this repository, and makes things easier. Git can be downloaded from the following link: 

[Git Download Page](https://git-scm.com/download/win)

### Cloning the repository

Open a command prompt into a known directory in your computer, such as Documents, and execute the follwing command: 

`git clone --recursive https://bitbucket.org/erjohnson227/vhdl-flip-flops.git`

### Starting the Container

To start the GHDL container, simply execute the following command in the root directory: 
```run_devenv.bat```

This will check to see if a docker image has already been built for GHDL or not, if it hasn't, it will build the image now. Note, depending on your internet connection and computer performance, this step may take up to 15 minutes to complete. Once completed the first time, subsequent runs will be very fast. 

If you have completed the prerequisite step of generating an SSH key, then you will be automatically logged into the docker instance at this time. If you have not setup an SSH key, you will be prompted for a password, the root password for the `ghdl_docker` image is "ghdl". Enter the password and hit enter, you will be logged in. 

### Running the Tests

Enter the build directory by typing the following command: 

`cd VHDL/build` 

Execute the unit tests by entering: 

`make test` 

This will build the testbench files and library dependencies and then run the testbences. If errors occur in the execution of the testbench files, the name of the module that failed will be listed in the output of the `make test` command. Full output logs of the testbench files are located in the `logs` folder, with one log per testbench, for example `output\sr_flipflop_tb.out` would be the logfile for the `sr_flipflop_tb` testbench. 

In addition to the testbench logs, value change dump (VCD) files are also created for each testbench, and are stored int the output folder. 

### Viewing the Test results in GTKWave
The `ghdl_docker` image contains GTKWave, and assuming that you followed the prerequisite steps and installed and configured VcXsrv, you can open GTKWave by simply executing the command `gtkwave`. It may be of more benefit to execute GTKWave to open a file directly from the build directory, and to run it in the background as to not block the terminal. This can be accomplished in the following command: 

`(gtkwave ../output/[filename].vcd 2&>1 > /dev/null &)`

where `[filename].vcd` is the name of the testbench you would like to view. 

### Killing the Container
A running instance of the GHDL development container may be killed by executing the `run_devenv` script with the `\k` switch as follows: 
`run_devenv.bat \k`

The image will then be destroyed, and the next call to `run_devenv` will create a new instance of the ghdl_docker image. 

### Forcing a Rebuild
Since the script only builds the image when the `ghdl_docker` image is not found in the docker image list, it may become necessary to force a rebuild of the image. This may be useful in cases where you have modified the Dockerfile. To force a rebuild of the docker image, execute the `run_devenv` script with the `\b` switch as follows: 

`run_devenv.bat \b` 

This will force a rebuild of the image before then starting an instance of the `ghdl_docker` image. 