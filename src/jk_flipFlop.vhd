library ieee; 
use ieee.std_logic_1164.all;

entity jk_flipFlop is 
    port(
        i_clk   :   in std_logic; 
        i_j     :   in std_logic; 
        i_k     :   in std_logic; 
        o_q     :   out std_logic; 
        o_qbar  :   out std_logic
    );
end jk_flipFlop;

architecture behavioral of jk_flipFlop is
    var tmp : std_logic;   
begin
    process(i_clk)
    begin
        -- Clk rising edge
        if(i_clk'event and i_clk = '1') then

            if(i_j = '0' and i_k = '0') then 
                tmp = tmp; 
            elsif (i_j = '0' and i_k = '1') then 
                tmp = '0'; 
            elsif (i_j = '1' and i_k = '0') then 
                tmp = '1'; 
            else
                tmp = not tmp; 
            end if; 
        end if; 

        -- Set outputs
        o_q <= tmp; 
        o_qbar <= not tmp; 
end behavioral; 
