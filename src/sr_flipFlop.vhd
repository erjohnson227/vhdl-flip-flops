library ieee; 
use ieee.std_logic_1164.all; 

entity sr_flipFlop is 
    port(
        i_s     :   in std_logic; 
        i_r     :   in std_logic;
        o_q     :   out std_logic;
        o_qbar  :   out std_logic 
    );
end sr_flipFlop; 

architecture behavioral of sr_flipFlop is
begin
    -- Async, no clock involved. 
    process(i_s,i_r)
    variable tmp: std_logic; 
    begin
        if(i_s = '0' and i_r = '0') then 
            tmp := tmp; 
        elsif(i_s = '0' and i_r = '1') then 
            tmp := '0'; 
        elsif(i_s = '1' and i_r = '0') then 
            tmp := '1'; 
        else 
            tmp := 'Z'; 
        end if; 
        
        -- Set outputs
        o_q <= tmp; 
        o_qbar <= not tmp; 
    end process;
end behavioral;