library ieee; 
use ieee.std_logic_1164.all;

entity d_flipFlop is 
    port(
        i_clk   :   in std_logic; 
        i_d     :   in std_logic; 
        o_q     :   out std_logic; 
        o_qbar  :   out std_logic; 
    ); 
end d_flipFlop; 

architecture behavioral of d_flipFlop is
begin
    process(i_clk)
        variable tmp : std_logic; 
        begin
            -- Clk rising edge
            if(i_clk'event and i_clk = '1') then 
                if (i_d = '1') then 
                    tmp = '1'; 
                else 
                    tmp = '0'; 
                end if; 

                -- Set oututs
                o_q <= tmp; 
                o_qbar <= not tmp; 
            end if; 
end behavioral; 