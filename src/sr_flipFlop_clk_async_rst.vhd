library ieee; 
use ieee.std_logic_1164.all;

entity sr_flipFlop_clk_async_rst is 
    port(
        i_clk   :   in std_logic; 
        i_s     :   in std_logic; 
        i_r     :   in std_logic;
        i_rst   :   in std_logic; 
        o_q     :   out std_logic;
        o_qbar  :   out std_logic  
    );
end sr_flipFlop_clk_async_rst; 

architecture behavioral of sr_flipFlop_clk_async_rst is 
begin

    process(i_clk, i_rst)
        variable tmp : std_logic; 
        begin
            -- Reset always takes priority
            if(i_rst = '1') then
                tmp := '0'; 

            -- Clk rising edge trigger
            elsif(i_clk'event and i_clk = '1') then
                if (i_s = '0' and i_r = '0') then 
                    tmp := tmp; 
                elsif (i_s = '0' and i_r = '1') then 
                    tmp := '0'; 
                elsif (i_s = '1' and i_r = '0') then 
                    tmp := '1'; 
                else 
                    tmp := 'Z'; 
                end if; 
            end if; 

            -- Set outputs
            o_q <= tmp; 
            o_qbar <= not tmp; 
        end process; 
end behavioral; 