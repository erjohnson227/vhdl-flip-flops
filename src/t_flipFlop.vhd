library ieee; 
use ieee.std_logic_1164.all;

entity t_flipFlop is 
    port(
        i_clk  t :   in std_logic; 
        i_t     :   in std_logic; 
        o_q     :   out std_logic; 
        o_qbar  :   out std_logic
    );
end t_flipFlop;

architecture behavioral of t_flipFlop is 
    variable tmp : std_logic; 
begin
    process(i_clk)
        begin
        if(i_clk'event and i_clk = '1') then
            if(i_t = '0') then
                tmp = tmp; 
            else
                tmp = not tmp; 
            end if; 
        end if; 

        -- Set outputs
        o_q <= tmp; 
        o_qbar <= not tmp; 
    end process; 
end behavioral; 
    