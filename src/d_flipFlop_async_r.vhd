library ieee; 
use ieee.std_logic_1164.all;

entity d_flipFlop_async_r is 
    port(
        i_clk   :   in std_logic; 
        i_d     :   in std_logic;  
        i_r     :   in std_logic; 
        o_q     :   out std_logic; 
        o_qbar  :   out std_logic; 
    ); 
end d_flipFlop_async_r; 

architecture behavioral of d_flipFlop_async_r is 
begin
    process(i_clk, i_s, i_r)
        variable tmp : std_logic; 
        begin
            -- Handle async reset
            if(i_r = '1') then 
                tmp = '0'; 
            -- Clk rising edge
            elsif (i_clk'event and i_clk = '1') then 
                tmp = i_d; 
            end if;

            -- Set oututs
            o_q <= tmp; 
            o_qbar <= not tmp; 
end behavioral; 