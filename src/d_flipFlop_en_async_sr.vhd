library ieee; 
use ieee.std_logic_1164.all;

entity d_flipFlop_en_async_sr is 
    port(
        i_clk   :   in std_logic; 
        i_en    :   in std_logic; 
        i_d     :   in std_logic; 
        i_s     :   in std_logic; 
        i_r     :   in std_logic; 
        o_q     :   out std_logic; 
        o_qbar  :   out std_logic; 
    ); 
end d_flipFlop_en_async_sr; 

architecture behavioral of d_flipFlop_en_async_sr is 
begin
    process(i_clk, i_s, i_r)
        variable tmp : std_logic; 
        begin
            -- Handle async events
            if(i_s = '0' and i_r = '1') then 
                tmp = '0'; 
            elsif (i_s = '1' and i_r = '0') then 
                tmp = '1'; 
            elsif (i_s = '1' and i_r = '1') then 
                tmp = 'Z'; 

            -- Clk rising edge
            elsif (i_clk'event and i_clk = '1') then 
                if(i_en = '1') then
                    tmp = i_d; 
                else
                    tmp = tmp; 
                end if; 
            end if;

            -- Set oututs
            o_q <= tmp; 
            o_qbar <= not tmp; 
end behavioral; 