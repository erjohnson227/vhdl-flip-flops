LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity sr_flipFlop_tb is 
end sr_flipFlop_tb; 


architecture behavioral of sr_flipFlop_tb is 
    component sr_flopFlop is 
        port(
            i_s     :   in std_logic; 
            i_r     :   in std_logic;
            o_q     :   out std_logic;
            o_qbar  :   out std_logic 
        );
    end component; 

    for sr_flipFlop_0 : sr_flopFlop use entity work.sr_flipFlop; 

    signal s        : std_logic := '0'; 
    signal r        : std_logic := '1'; 
    signal q, qbar  : std_logic; 

    constant clk_period : time := 20 ns; 

    begin
        sr_flipFlop_0 : sr_flopFlop port map(
                                i_s => s, 
                                i_r => r, 
                                o_q => q, 
                                o_qbar => qbar); 
    

        process
        begin

            wait for clk_period; 

            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            wait for clk_period; 

            r <= '0'; 

            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            wait for clk_period; 

            -- Check set 
            s <= '1'; 

            wait for clk_period/2; 
            -- Check for set
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error; 

            s <= '0';

            wait for clk_period/2; 
            wait for clk_period; 

            -- Check for set
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error; 

            r <= '1'; 

            wait for clk_period/2; 

            -- Check for unset
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            r <= '0'; 

            wait for clk_period/2; 
            wait for clk_period; 

            -- Check for unset
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            r <= '1'; 
            s <= '1'; 

            wait for clk_period/2; 

            -- Check for Z
            assert q = 'Z' report "Expected q = Z" 
                severity error; 

            assert qbar = 'X' report "Expeced qbar = X" 
                severity error; 

            assert false report "End of test" severity note; 
            wait; 
        end process;
    end behavioral; 