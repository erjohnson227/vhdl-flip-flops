LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity sr_flipFlop_clk_async_rst_tb is
end sr_flipFlop_clk_async_rst_tb; 

architecture behavioral of sr_flipFlop_clk_async_rst_tb is
    component sr_flipFlop_clk_async_rst is 
        port(
            i_clk   :   in std_logic; 
            i_s     :   in std_logic; 
            i_r     :   in std_logic;
            i_rst   :   in std_logic; 
            o_q     :   out std_logic;
            o_qbar  :   out std_logic  
        );
    end component; 

    for sr_flipFlop_clk_async_rst_0   : sr_flipFlop_clk_async_rst use entity work.sr_flipFlop_clk_async_rst;

    signal clk      : std_logic := '0'; 
    signal s        : std_logic := '0'; 
    signal r        : std_logic := '0'; 
    signal rst      : std_logic := '1'; 
    signal q        : std_logic; 
    signal qbar     : std_logic; 

    constant clk_period : time := 20 ns; 

    begin 
        sr_flipFlop_clk_async_rst_0 : sr_flipFlop_clk_async_rst port map (i_clk => clk, 
                                                      i_s => s, 
                                                      i_r => r,
                                                      i_rst => rst,
                                                      o_q => q, 
                                                      o_qbar => qbar); 
        
        -- TIck the clock
        clk_process: process
        begin
            for i in 0 to 15 loop
                clk <= '0'; 
                wait for clk_period/2; 
                clk <= '1'; 
                wait for clk_period/2; 
            end loop;
            wait; 
        end process clk_process;

        stim_proc: process
        begin

            wait for clk_period; 
            
            -- Check to ensure power on reset. 
            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 
                
            rst <= '0'; 

            -- Wait one cycle 
            wait for clk_period; 

            -- Check to ensure persistent unset state. 
            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            -- Verify set
            s <= '1'; 
            wait for clk_period; 

            -- Check to ensure transition to set state. 
            -- Check for set state
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error; 

            s <= '0';

            wait for clk_period; 

            -- Check to ensure persistent set state. 
            -- Check for set
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error; 

            -- Transition to unset state
            r <= '1'; 

            -- Test transitoin to unset
            -- Check for unset
            assert q = '1' report "Expected q = 0" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 1" 
                severity error; 

            wait for clk_period; 

            r <= '0'; 

            wait for clk_period; 
            
            s <= '1'; 

            wait for clk_period/2; 

            -- Ensure set only on rising edge. 
            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            s <= '0'; 

            -- Second half of test, ensure set only on rising edge of clk. 
            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            wait for clk_period/2; 

            wait for clk_period; 

            -- Transition to set 
            s <= '1'; 

            wait for clk_period; 

            s <= '0'; 

            wait for clk_period; 

            -- Ensure unset only on rising edge of clk
            r <= '1'; 
 
            wait for clk_period/2; 

            -- Check for set state
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error; 

            r <= '0'; 
            
            -- Part 2, ensure still set; 
            -- Check for set state
            assert q = '1' report "Expected q = 1" 
                severity error; 

            assert qbar = '0' report "Expeced qbar = 0" 
                severity error;  

            wait for clk_period; 

            -- Test async reset. 
            rst <= '1'; 

            wait for clk_period/2; 

            -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 
                
            rst <= '0'; 

            wait for clk_period/2; 

           -- Check for unset state
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            -- Check for unset
            assert q = '0' report "Expected q = 0" 
                severity error; 

            assert qbar = '1' report "Expeced qbar = 1" 
                severity error; 

            r <= '1'; 
            s <= '1'; 

            wait for clk_period/2; 

            -- Check for Z
            assert q = 'Z' report "Expected q = Z" 
                severity error; 

            assert qbar = 'X' report "Expeced qbar = X" 
                severity error;
                
            assert false report "End of test" severity note; 
            wait; 
        end process;
end behavioral; 
